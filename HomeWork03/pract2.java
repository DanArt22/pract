import java.util.Scanner;

public class pract2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int digitsSum = 0;
        int min = 2147483647;
        int nub;
        int small=0;
        while (number != -1) {
            nub = number;
            while (number != 0) {
                int lastDigit = number % 10;
                number = number / 10;
                digitsSum = digitsSum + lastDigit;
            }
            if (digitsSum < min) {
                small = nub;
                min = digitsSum;

            }
            digitsSum = 0;
            number = scanner.nextInt();
        }
        System.out.println("Минимальная сумма цифр находится в числе " + small);
    }
}