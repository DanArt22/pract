import java.util.Scanner;

public class pract3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int lmin = 0;
        int f = scanner.nextInt();
        int s = scanner.nextInt();
        int th = scanner.nextInt();
        while (th != -1) {
            if (s < f && s < th) {
                lmin++;
            }
            f = s;
            s = th;
            th = scanner.nextInt();
        }
        System.out.println("Количество локальных минимумов " + lmin);
    }
}
